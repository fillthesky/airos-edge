def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "paho",
        build_file = clean_dep("//third_party/paho:paho.BUILD"),
        path = "/opt/paho"
    )