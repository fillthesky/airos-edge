def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "dahua-sdk",
        build_file = clean_dep("//third_party/dahua-sdk:dahua-sdk.BUILD"),
        path = "/opt/dahua-sdk"
    )