def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "asn_wrapper",
        build_file = clean_dep("//third_party/asn_wrapper:asn_wrapper.BUILD"),
        path = "/opt/asn_wrapper"
    )