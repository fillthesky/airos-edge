"""Load the video_decoder library"""

# Sanitize a dependency so that it works correctly from code that includes
# Apollo as a submodule.
def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "video_decoder",
        build_file = clean_dep("//third_party/video_decoder:video_decoder.BUILD"),
        path = "/opt/video_decoder",
    )