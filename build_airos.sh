#! /usr/bin/env bash
set -e

TOP_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd -P)" # 得到当前脚本执行的目录

# 获取当前系统的CPU架构，存入ARCH变量
ARCH="$(uname -m)"

# 支持的CPU架构列表
SUPPORTED_ARCHS=" x86_64 aarch64 "

# 定义支持的命令列表，用于帮助信息和命令行参数判断
AVAILABLE_COMMANDS="build clean -h --help"

# 定义支持的设备类型列表，分别属于不同的分类
SUPPORTED_DEVICE_CONNECT=(rsu cloud traffic_light ins radar lidar)
SUPPORTED_TEST_TOOL_DEVICE=(traffic_light lidar ins)
SUPPORTED_DEVICE_SERVICE=(traffic_light rsu cloud)

# 定义协议栈组件列表
PROTOCOL_STACK_COMPONENT=(traffic_light_adapter rsi_generator perception_adapter v2x_message_reporter v2x_codec)

# 定义输出目录结构，分为不同的子目录
OUT_ROOT="${TOP_DIR}/output" # 根目录
OUT_3RD="${OUT_ROOT}/3rd" # 第三方库输出目录
OUT_TEST="${OUT_ROOT}/test" # 测试相关输出目录
OUT_TOOL="${OUT_ROOT}/tool" # 工具相关输出目录
OUT_PROTO="${OUT_ROOT}/protobuf" # protobuf相关输出目录

# 定义一些基础路径变量，这些变量通常被各个模块使用
base_path="base" # base路径
modules_path="modules" # modules路径
perception_camera_path="air_service/modules/perception-camera" # 感知相机模块
perception_viz_path="air_service/modules/perception-visualization" # 感知可视化模块
perceptiio_usecase_path="air_service/modules/perception-usecase" # 感知事件模块
perception_fusion_path="air_service/modules/perception-fusion" # 感知融合模块
device_connect_path="base/device_connect" # 设备连接模块
device_service_path="middleware/device_service" # 设备服务模块
protocol_path="middleware/protocol" # 协议栈模块

# 检查架构是否受支持
function check_architecture_support() {
    # 检查当前支持的架构列表中是否包含ARCH
    if [[ "${SUPPORTED_ARCHS}" != *" ${ARCH} "* ]]; then
        # 输出不支持的CPU架构信息，并列出当前支持的架构
        echo "Unsupported CPU arch: ${ARCH}. Currently, AIROS only" \
            # 目前，AIROS仅支持在以下CPU架构上运行
            "supports running on the following CPU archs:"
        echo "${TAB}${SUPPORTED_ARCHS}"
        # 退出程序，返回状态码1
        exit 1
    fi
}

# 检查平台是否受支持
function check_platform_support() {
    # 获取当前操作系统平台
    local platform="$(uname -s)"
    # 如果平台不是Linux
    if [[ "${platform}" != "Linux" ]]; then
        # 输出不支持的平台信息
        echo "Unsupported platform: ${platform}."
        echo "${TAB}AIROS is expected to run on Linux systems (E.g., Debian/Ubuntu)."
        # 退出程序，返回状态码1
        exit 1
    fi
}

# 检查内存最小需求
function check_minimal_memory_requirement() {
    # 定义最小内存需求，单位为GB，2.0GB即20736MB
    local minimal_mem_gb="2.0"
    # 使用free命令获取系统内存，然后通过awk获取总内存大小并转换为GB
    local actual_mem_gb="$(free -m | awk '/Mem:/ {printf("%0.2f", $2 / 1024.0)}')"
    # 通过bc计算实际内存是否小于最小内存需求
    if (($(echo "$actual_mem_gb < $minimal_mem_gb" | bc -l))); then
        # 如果实际内存小于最小内存需求，输出警告信息
        echo "System memory [${actual_mem_gb}G] is lower than the minimum required" \
            "[${minimal_mem_gb}G]. AIROS build could fail."
    fi
}

# 环境设置
function env_setup() {
    # 检查架构是否受支持
    check_architecture_support
    # 检查平台是否受支持
    check_platform_support
    # 检查内存最小需求
    check_minimal_memory_requirement
}

# 编译和发布protobuf相关的构建，并将结果复制到输出目录
function release_protobuf {

    # 使用Bazel构建工具执行三个构建任务：protoc，protobuf，和protobuf_lite。@符号表示这些是外部依赖项。
    bazel build @com_google_protobuf//:protoc # 编译protoc，用于生成protobuf代码
    bazel build @com_google_protobuf//:protobuf # 编译protobuf库，包含C++和其他语言的接口
    bazel build @com_google_protobuf//:protobuf_lite # 编译protobuf轻量级库，只包含C++接口

    # 定义编译完成的protobuf二进制文件的路径、输出目录。
    local proto_buf="${TOP_DIR}/bazel-bin/external/com_google_protobuf"
    local proto_out=${OUT_PROTO}

    # 检查输出目录是否存在，如果不存在则创建。
    [ -d "${proto_out}" ] || mkdir -p ${proto_out}

    # 检查是否存在特定的文件，如果存在则复制到输出目录。
    # ${proto_buf}是包含编译完成的protoc，protobuf和protobuf_lite的目录。
    [ -e "${proto_buf}/protoc" ] && cp -f ${proto_buf}/protoc ${proto_out}/ # 复制protoc到输出目录
    [ -e "${proto_buf}/libprotobuf.so" ] && cp -f ${proto_buf}/libprotobuf.so ${proto_out}/ # 复制protobuf库到输出目录
    [ -e "${proto_buf}/libprotobuf_lite.so" ] && cp -f ${proto_buf}/libprotobuf_lite.so ${proto_out}/ # 复制protobuf轻量级库到输出目录

    # 检查第三方的protobuf版本库中是否存在.include目录，如果存在则复制到输出目录的对应位置。
    [ -e "${TOP_DIR}/third_party/protobuf/.include" ] && mkdir -p ${proto_out}/include/google/protobuf && \
        cp -rf ${TOP_DIR}/third_party/protobuf/.include/* ${proto_out}/include/google/protobuf/ # 复制.include目录到输出目录
}

# 发布airosrt相关的文件
# 如需自定义编译流程或编译x86_64以外的平台，请下载源码并参照apollo项目构建相应平台镜像进行编译
# apollo项目：https://github.com/ApolloAuto/apollo
# cyberRT源码：https://zhilu.bj.bcebos.com/cyberRT.tar.gz
function release_airosrt() {
    # 定义二进制文件存放目录的路径、配置文件存放目录的路径
    local bin_dir="${OUT_ROOT}/airosrt/bin"
    local conf_dir="${OUT_ROOT}/airosrt/cyber"

    # 创建二进制文件存放目录和配置文件存放目录，如果目录已存在则不进行创建
    mkdir -p ${bin_dir}
    mkdir -p ${conf_dir}

    # 如果在/opt/airosrt/bin目录存在，则复制该目录下的所有文件到定义的bin_dir目录下
    [ -d "/opt/airosrt/bin" ] && \
        cp -rf /opt/airosrt/bin/* ${bin_dir}/

    # 如果在/opt/airosrt/cyber目录存在，则复制该目录下的所有文件到定义的conf_dir目录下
    [ -d "/opt/airosrt/cyber" ] && \
        cp -rf /opt/airosrt/cyber/* ${conf_dir}/

    # 在conf_dir的父目录下创建一个__init__.py文件，该文件在Python中被视为包的主文件，使得Python可以识别这个目录为一个包
    touch ${conf_dir}/../__init__.py
}


# 构建设备接入的protobuf定义
function build_base_device_connect_pb() {
    # 对于支持的每个设备
    for device in ${SUPPORTED_DEVICE_CONNECT[@]}
    do
        # 如果设备的proto文件存在
        if [ -f "${TOP_DIR}/${device_connect_path}/proto/${device}_data.proto" ]; then
            # 使用bazel构建该proto文件对应的编译文件
            bazel build "//${device_connect_path}/proto:${device}_data_cc_pb"
        fi
    done
}

# 发布设备接入的protobuf定义
function release_base_device_connect_pb() {
    # 对于支持的每个设备
    for device in ${SUPPORTED_DEVICE_CONNECT[@]}
    do
        # 定义输出目录的临时变量
        local tmp_out="${OUT_ROOT}/${device_connect_path}/${device}"
        # 创建或检查包含头文件的目录
        local tmp_out_inc="${tmp_out}/include"
        [ -d "${tmp_out_inc}" ] || mkdir -p ${tmp_out_inc}
        # 创建或检查库文件的目录
        local tmp_out_lib="${tmp_out}/lib"
        [ -d "${tmp_out_lib}" ] || mkdir -p ${tmp_out_lib}
        # 创建或检查proto文件的目录
        local tmp_out_proto="${tmp_out}/proto"
        [ -d "${tmp_out_proto}" ] || mkdir -p ${tmp_out_proto}

        # 如果设备的proto文件存在
        if [ -f "${TOP_DIR}/${device_connect_path}/proto/${device}_data.proto" ]; then
            # 复制生成的protobuf头文件到输出目录的include目录下
            cp -f ${TOP_DIR}/bazel-bin/${device_connect_path}/proto/${device}_data.pb.h ${tmp_out_inc}
            # 复制生成的protobuf库文件到输出目录的lib目录下
            cp -f ${TOP_DIR}/bazel-bin/${device_connect_path}/proto/lib${device}_data_pb.so ${tmp_out_lib}
            # 复制原始的proto文件到输出目录的proto目录下
            cp -f ${TOP_DIR}/${device_connect_path}/proto/${device}_data.proto   ${tmp_out_proto}
        fi
    done
}

# 构建设备接入模块
function build_base_device_connect() {
    # 对于支持的每个设备
    for device in ${SUPPORTED_DEVICE_CONNECT[@]}
    do
        # 使用bazel构建该设备的编译目标
        bazel build "//${device_connect_path}/${device}:${device}_device"
    done
}

# 发布设备接入模块
function release_base_device_connect() {
    # 对于支持的每个设备
    for device in ${SUPPORTED_DEVICE_CONNECT[@]}
    do
        # 定义输出目录的临时变量
        local tmp_out="${OUT_ROOT}/${device_connect_path}/${device}"
        # 创建或检查包含头文件的目录
        local tmp_out_inc="${tmp_out}/include"
        [ -d "${tmp_out_inc}" ] || mkdir -p ${tmp_out_inc}
        # 创建或检查库文件的目录
        local tmp_out_lib="${tmp_out}/lib"
        [ -d "${tmp_out_lib}" ] || mkdir -p ${tmp_out_lib}

        # 复制设备库文件到输出目录的库文件目录下
        cp -f ${TOP_DIR}/bazel-bin/${device_connect_path}/${device}/lib${device}_device.so ${tmp_out_lib}
        # 复制基础设备头文件到输出目录的包含头文件目录下
        cp -f ${TOP_DIR}/${device_connect_path}/${device}/device_base.h ${tmp_out_inc}
        # 复制设备工厂头文件到输出目录的包含头文件目录下
        cp -f ${TOP_DIR}/${device_connect_path}/${device}/device_factory.h ${tmp_out_inc}

        # 定义依赖库的路径
        depends_lib="{TOP_DIR}/${device_connect_path}/${device}/lib/"
        # 如果依赖库的目录存在
        if [ -d ${depends_lib} ]; then
            # 使用find命令找到依赖库中的所有.so文件并复制到输出目录的库文件目录下
            find ${depends_lib}/ -name "lib*.so*" | xargs -I {} cp -f {} ${tmp_out_lib}/
        fi
    done
}

# 构建设备接入模块的单元测试
function build_base_device_connect_ut() {
    # 遍历所有支持的设备
    for device in ${SUPPORTED_DEVICE_CONNECT[@]}
    do
        # 使用bazel为每个设备构建对应的单元测试目标
        bazel build "//${device_connect_path}/${device}:${device}_ut"
    done
}

# 发布设备接入模块的单元测试
function release_base_device_connect_ut() {
    # 遍历所有支持的设备
    for device in ${SUPPORTED_DEVICE_CONNECT[@]}
    do
        # 定义输出目录的临时变量
        local tmp_out="${OUT_ROOT}/${device_connect_path}/${device}"
        # 定义单元测试目录的临时变量
        local tmp_out_ut="${tmp_out}/ut"
        # 检查单元测试目录是否存在，不存在则创建
        [ -d "${tmp_out_ut}" ] || mkdir -p ${tmp_out_ut}
        # 将设备对应的单元测试二进制文件复制到输出目录的单元测试目录下
        cp -f ${TOP_DIR}/bazel-bin/${device_connect_path}/${device}/${device}_ut ${tmp_out_ut}
    done
}

# 构建设备接入模块的工具
function build_base_device_connect_tool() {
    # 遍历所有支持的测试工具设备
    for device in ${SUPPORTED_TEST_TOOL_DEVICE[@]}
    do
        # 使用bazel为每个测试工具设备构建对应的工具目标
        bazel build "//${device_connect_path}/${device}:${device}_test_tool"
    done
}

# 发布设备接入模块的工具
function release_base_device_connect_tool() {
    # 遍历所有支持的测试工具设备
    for device in ${SUPPORTED_TEST_TOOL_DEVICE[@]}
    do
        # 定义输出目录的临时变量
        local tmp_out="${OUT_ROOT}/${device_connect_path}/${device}"
        # 定义工具目录的临时变量
        local tmp_out_test_tool="${tmp_out}/test_tool"
        # 检查工具目录是否存在，不存在则创建
        [ -d "${tmp_out_test_tool}" ] || mkdir -p ${tmp_out_test_tool}
        # 将设备对应的工具二进制文件复制到输出目录的工具目录下
        cp -f ${TOP_DIR}/bazel-bin/${device_connect_path}/${device}/${device}_test_tool ${tmp_out_test_tool}
    done
}

# 构建设备接入模块
function build_device_connect_func() {
    # 构建设备接入的protobuf
    build_base_device_connect_pb
    # 构建设备接入
    build_base_device_connect
    # 构建设备接入单元测试
    build_base_device_connect_ut
    # 构建设备接入工具
    build_base_device_connect_tool
}

# 发布设备接入
function release_base_device_connect_func() {
    # 发布设备接入的protobuf
    release_base_device_connect_pb
    # 发布基设备接入
    release_base_device_connect
    # 发布设备接入单元测试
    release_base_device_connect_ut
    # 发布设备接入工具
    release_base_device_connect_tool
}

# 运行设备接入单元测试
function run_base_device_connect_ut() {
    # 对于支持的每种设备连接类型
    for device in ${SUPPORTED_DEVICE_CONNECT[@]}
    do
        # 切换到对应的设备连接目录
        pushd ./bazel-bin/${device_connect_path}/${device} > /dev/null
            # 运行该设备的单元测试
            ./${device}_ut
        # 切换回原目录
        popd > /dev/null  
    done
    echo "base device connect ut done!"
}

# 构建设备服务Protobuf
function build_middleware_device_service_pb() {
    # 遍历支持的设备服务列表
    for device in ${SUPPORTED_DEVICE_SERVICE[@]}
    do
        # 如果对应的设备配置Protobuf文件存在
        if [ -f "${TOP_DIR}/${device_service_path}/proto/${device}_config.proto" ]; then
            # 使用bazel构建该设备配置的Protobuf
            bazel build //${device_service_path}/proto:${device}_config_cc_pb
        fi
    done
}

# 发布设备服务Protobuf
function release_middleware_device_service_pb() {
    # 遍历支持的设备服务列表
    for device in ${SUPPORTED_DEVICE_SERVICE[@]}
    do
        # 定义输出目录变量
        local tmp_out="${OUT_ROOT}/${device_service_path}/${device}"
        # 定义包含头文件目录变量
        local tmp_out_inc="${tmp_out}/include"
        # 检查头文件目录是否存在，不存在则创建
        [ -d "${tmp_out_inc}" ] || mkdir -p ${tmp_out_inc}
        # 定义库文件目录变量
        local tmp_out_lib="${tmp_out}/lib"
        # 检查库文件目录是否存在，不存在则创建
        [ -d "${tmp_out_lib}" ] || mkdir -p ${tmp_out_lib}
        # 定义Proto文件目录变量
        local tmp_out_proto="${tmp_out}/proto"
        # 检查Proto文件目录是否存在，不存在则创建
        [ -d "${tmp_out_proto}" ] || mkdir -p ${tmp_out_proto}

        # 如果设备数据Protobuf文件存在
        if [ -f "${TOP_DIR}/${device_connect_path}/proto/${device}_data.proto" ]; then
            # 复制构建的设备配置Protobuf头文件到目标目录
            cp -f ${TOP_DIR}/bazel-bin/${device_service_path}/proto/${device}_config.pb.h ${tmp_out_inc}
            # 复制构建的设备配置Protobuf库文件到目标目录
            cp -f ${TOP_DIR}/bazel-bin/${device_service_path}/proto/lib${device}_config_pb.so ${tmp_out_lib}
            # 复制设备配置Proto文件到目标目录
            cp -f ${TOP_DIR}/${device_service_path}/proto/${device}_config.proto ${tmp_out_proto}
        fi
    done
}

# 构建设备服务
function build_middleware_device_service() {
    # 遍历支持的设备列表
    for device in ${SUPPORTED_DEVICE_SERVICE[@]}
    do
        # 使用bazel构建该设备的lib组件
        bazel build "//${device_service_path}/${device}:lib${device}_component.so"
    done
}

# 发布设备服务
function release_middleware_device_service() {
    # 遍历支持的设备列表
    for device in ${SUPPORTED_DEVICE_SERVICE[@]}
    do
        # 定义输出目录变量
        local tmp_out="${OUT_ROOT}/${device_service_path}/${device}"

        # 定义库文件目录变量
        local tmp_out_lib="${tmp_out}/lib"
        # 检查库文件目录是否存在，不存在则创建
        [ -d "${tmp_out_lib}" ] || mkdir -p ${tmp_out_lib}

        # 定义so文件路径
        local so_path="${TOP_DIR}/bazel-bin/${device_service_path}/${device}/lib${device}_component.so"
        # 检查so文件是否存在，如果存在则复制到目标库文件目录
        if [ -e "${so_path}" ];then 
            cp -f ${so_path} ${tmp_out_lib}
        fi

        # 定义二进制文件目录变量
        local tmp_out_bin="${tmp_out}/bin"
        # 检查二进制文件目录是否存在，不存在则创建
        [ -d "${tmp_out_bin}" ] || mkdir -p ${tmp_out_bin}

        # 定义二进制文件路径
        local bin_path="${TOP_DIR}/bazel-bin/${device_service_path}/${device}/${device}_component"
        # 检查二进制文件是否存在，如果存在则复制到目标二进制文件目录
        if [ -e "${bin_path}" ];then 
            cp -f ${bin_path} ${tmp_out_bin}
        fi

        # 定义依赖库目录路径
        depends_lib="{TOP_DIR}/${device_service_path}/${device}/lib/"
        # 检查依赖库目录是否存在，如果存在则复制其中的所有lib*.so*到目标库文件目录
        if [ -d ${depends_lib} ]; then
            find ${depends_lib}/ -name "lib*.so*" | xargs -I {} cp -f {} ${tmp_out_lib}/
        fi
    done
}

# 构建设备服务单元测试
function build_middleware_device_service_ut() {
    # 使用bazel构建针对traffic_light设备的traffic_light_service_ut单元测试
    bazel build "//${device_service_path}/traffic_light:traffic_light_service_ut"
}

# 发布设备服务单元测试
function release_middleware_device_service_ut() {
    # 定义输出目录变量
    local tmp_out="${OUT_ROOT}/${device_service_path}/traffic_light"
    # 定义单元测试文件目录
    local tmp_out_ut="${tmp_out}/ut"
    # 检查单元测试文件目录是否存在，如果不存在则创建
    [ -d "${tmp_out_ut}" ] || mkdir -p ${tmp_out_ut}
    # 将编译得到的针对traffic_light设备的traffic_light_service_ut单元测试复制到目标单元测试文件目录
    cp -f ${TOP_DIR}/bazel-bin/${device_service_path}/traffic_light/traffic_light_service_ut ${tmp_out_ut}
    # 定义测试配置文件目录
    local tmp_cfg_dir="${TOP_DIR}/${device_service_path}/traffic_light/ut/testdata/ "
    # 检查测试配置文件目录是否存在，如果存在则复制到目标单元测试文件目录
    [ -d ${tmp_cfg_dir} ] && cp -rf ${tmp_cfg_dir} ${tmp_out_ut}
}

# 构建设备服务模块
function build_middleware_device_service_func() {
    # 构建protobuf服务
    build_middleware_device_service_pb
    # 构建设备服务
    build_middleware_device_service
    # 构建设备服务单元测试
    build_middleware_device_service_ut
}

# 发布设备服务模块
function release_middleware_device_service_func() {
    # 发布protobuf服务
    release_middleware_device_service_pb
    # 发布设备服务
    release_middleware_device_service
    # 发布设备服务单元测试
    release_middleware_device_service_ut
}

# 构建协议栈PB
function build_middleware_protocol_pb() {
    # 遍历所有proto文件
    for proto in ${protocol_path}/proto/*.proto;
    do
        # 使用bazel构建对应的cc_pb文件
        bazel build //${protocol_path}/proto:$(basename ${proto%.*})_cc_pb
    done
}

# 构建协议栈模块
function build_middleware_protocol() {
    # 遍历所有组件
    for component in ${PROTOCOL_STACK_COMPONENT[@]}
    do
        # 使用bazel构建对应组件的动态链接库（lib${component}.so）
        bazel build "//${protocol_path}/${component}:lib${component}.so"
    done
}

# 发布协议栈PB
function release_middleware_protocol_pb(){
    # 遍历所有组件
    for component in ${PROTOCOL_STACK_COMPONENT[@]}
    do
        # 定义输出目录变量
        local tmp_out="${OUT_ROOT}/${protocol_path}/${component}"
        # 定义头文件输出目录
        local tmp_out_inc="${tmp_out}/include"
        # 检查头文件输出目录是否存在，如果不存在则创建
        [ -d "${tmp_out_inc}" ] || mkdir -p ${tmp_out_inc}
        # 定义库文件输出目录
        local tmp_out_lib="${tmp_out}/lib"
        # 检查库文件输出目录是否存在，如果不存在则创建
        [ -d "${tmp_out_lib}" ] || mkdir -p ${tmp_out_lib}
        # 定义proto文件输出目录
        local tmp_out_proto="${tmp_out}/proto"
        # 检查proto文件输出目录是否存在，如果不存在则创建
        [ -d "${tmp_out_proto}" ] || mkdir -p ${tmp_out_proto}

    # 复制编译得到的所有.pb.h头文件到目标头文件输出目录
    cp -f ${TOP_DIR}/bazel-bin/${protocol_path}/proto/*.pb.h ${tmp_out_inc}
    # 复制编译得到的所有.so库文件到目标库文件输出目录
    cp -f ${TOP_DIR}/bazel-bin/${protocol_path}/proto/*.so ${tmp_out_lib}
    # 复制原始proto文件到目标proto文件输出目录
    cp -f ${TOP_DIR}/${protocol_path}/proto/*.proto ${tmp_out_proto}
    done
}

# 发布协议栈
function release_middleware_protocol(){
    # 遍历所有组件
    for component in ${PROTOCOL_STACK_COMPONENT[@]}
    do
        # 定义输出目录变量
        local tmp_out="${OUT_ROOT}/${protocol_path}/${component}"
        # 定义库文件输出目录
        local tmp_out_lib="${tmp_out}/lib"
        # 检查库文件输出目录是否存在，如果不存在则创建
        [ -d "${tmp_out_lib}" ] || mkdir -p ${tmp_out_lib}
        # 定义动态链接库路径
        local so_path="${TOP_DIR}/bazel-bin/${protocol_path}/${component}/lib${component}.so"
        # 如果动态链接库存在，则复制到目标库文件输出目录
        if [ -e "${so_path}" ];then
            cp -f ${so_path} ${tmp_out_lib}
        fi
    done
}

# 构建协议栈单元测试
function build_middleware_protocol_ut() {
    # 通过循环，遍历数组 PROTOCOL_STACK_COMPONENT 中的每一个元素，每一个元素代表一个组件
    for component in ${PROTOCOL_STACK_COMPONENT[@]}
    do
        # 使用 bazel 命令构建指定路径下的单元测试可执行文件，路径和名称由 component 变量决定
        bazel build "//${protocol_path}/${component}:${component}_ut"
    done
}

# 发布协议栈单元测试
function release_middleware_protocol_ut() {
    # 同样地，通过循环，遍历数组 PROTOCOL_STACK_COMPONENT 中的每一个元素
    for component in ${PROTOCOL_STACK_COMPONENT[@]}
    do
        # 定义临时输出目录，其路径和名称由 protocol_path 和 component 变量决定
        local tmp_out="${OUT_ROOT}/${protocol_path}/${component}"
        # 定义单元测试的临时输出目录
        local tmp_out_ut="${tmp_out}/ut"
        # 检查单元测试的临时输出目录是否存在，如果不存在，则创建该目录
        [ -d "${tmp_out_ut}" ] || mkdir -p ${tmp_out_ut}
        # 将构建好的单元测试可执行文件复制到临时输出目录中
        cp -f ${TOP_DIR}/bazel-bin/${protocol_path}/${component}/${component}_ut ${tmp_out_ut}
        # 定义单元测试配置文件的临时目录
        local tmp_cfg_dir="${TOP_DIR}/${protocol_path}/${component}/ut/testdata"
        # 如果配置文件目录存在，则将其复制到单元测试的临时输出目录中
        [ -d ${tmp_cfg_dir} ] && cp -rf ${tmp_cfg_dir} ${tmp_out_ut}
    done
}

# 构建协议栈模块
function build_middleware_protocol_func() {
    # 构建协议栈protobuf
    build_middleware_protocol_pb
    # 构建协议栈组件
    build_middleware_protocol
    # 构建协议栈单元测试
    build_middleware_protocol_ut
}

# 发布协议栈模块
function release_middleware_protocol_func() {
    # 发布协议栈protobuf 定义
    release_middleware_protocol_pb
    # 发布协议栈组件
    release_middleware_protocol
    # 发布协议栈单元测试
    release_middleware_protocol_ut
}


# 构建base模块
function build_base() {
    # 遍历在base目录下的每个模块
    for base_module in `ls ${TOP_DIR}/base`;do
        # 开始构建设备接入模块
        if [ "${base_module}" = "device_connect" ];then
            build_device_connect_func
            # continue 使得循环跳到下一个迭代，不执行后续的构建操作
            continue
        fi
        # 对于其他模块，使用 bazel 构建它们的代码，并启用 CUDA 规则
        bazel build //base/${base_module}/... --@rules_cuda//cuda:enable=True
    done
    # 单独为 device_connect 模块构建camera子模块，同样启用 CUDA 规则
    bazel build //base/device_connect/camera/... --@rules_cuda//cuda:enable=True
}

# 发布base模块的二进制文件
function release_base() {
    # 遍历每个模块
    for base_module in `ls ${TOP_DIR}/base`;do
        # 发布设备接入模块
        if [ "${base_module}" = "device_connect" ];then
            release_base_device_connect_func
            continue
        fi
        # 定义临时输出目录，如果不存在则创建，用于存放当前模块的二进制文件
        local tmp_out="${OUT_ROOT}/${base_path}"
        [ -d "${tmp_out}" ] || mkdir -p ${tmp_out}
        # 从 bazel-bin 目录中复制当前模块的所有 .so 文件到临时输出目录
        cp -f ${TOP_DIR}/bazel-bin/${base_path}/${base_module}/*.so ${tmp_out}
    done
    local tmp_out="${OUT_ROOT}/${base_path}"
    cp -f ${TOP_DIR}/bazel-bin/${base_path}/device_connect/camera/*.so ${tmp_out}
}

# 构建modules
function build_modules() {
    # 使用 bazel 构建工具，构建位于 //air_service/modules/... 的代码。//表示的是在项目根目录下的路径。
    # 这里构建的是所有的模块，... 是一个通配符，表示匹配该路径下的所有子目录和文件。
    bazel build //air_service/modules/... --@rules_cuda//cuda:enable=True   --local_cpu_resources=8 --local_ram_resources=1024  --copt -g --strip=never
}

# 从指定目录复制库文件
function copy_lib_from_dir() {
    # 列出一级目录下的所有文件和目录
    for file in ` ls $1`
    do
        if [ -d $1"/"$file ]
        then
            # 如果当前的文件或目录是一个目录，那么递归调用，复制该目录下的文件
            if [ "${file##*.}"x != "runfiles"x ]
            then
                copy_lib_from_dir $1"/"$file $2
            fi
        else
            if [ "${file##*.}"x = "so"x ]
            then
                # 定义本地变量 path 和 name，path 为当前文件的完整路径，name 为当前文件的文件名
                local path="$1/$file"
                local name=$file
                # 如果目标目录下不存在相同的文件，那么复制该文件到目标目录
                if [ ! -f $2"/"$name ]
                then
                    cp -f ${path} "$2/${name}"
                fi
            fi
        fi
    done
}

function release_modules() {
    local perception_camera_out="${OUT_ROOT}/${perception_camera_path}"
    [ -d "${perception_camera_out}/lib" ] || mkdir -p ${perception_camera_out}/lib
    # 调用 copy_lib_from_dir 函数，从 TOP_DIR/bazel-bin/perception_camera_path 路径复制库文件到 perception_camera_out/lib 目录
    copy_lib_from_dir ${TOP_DIR}/bazel-bin/${perception_camera_path} ${perception_camera_out}/lib
    cp -rf "${TOP_DIR}/${perception_camera_path}/dag" ${perception_camera_out}
    cp -rf "${TOP_DIR}/${perception_camera_path}/conf" ${perception_camera_out}

    [ -d "${perception_camera_out}/data" ] || mkdir -p ${perception_camera_out}/data
    cp -rf "${TOP_DIR}/${perception_camera_path}/algorithm/detector/air_detector/data" "${perception_camera_out}/data/detector/"
    cp -rf "${TOP_DIR}/${perception_camera_path}/algorithm/tracker/air_tracker/data" "${perception_camera_out}/data/tracker/"

    # visualization
    local perception_viz_out="${OUT_ROOT}/${perception_viz_path}"
    [ -d "${perception_viz_out}/lib" ] || mkdir -p ${perception_viz_out}/lib
    # 调用 copy_lib_from_dir 函数，从 TOP_DIR/bazel-bin/perception_viz_path 路径复制库文件到 perception_viz_out/lib 目录
    copy_lib_from_dir ${TOP_DIR}/bazel-bin/${perception_viz_path} ${perception_viz_out}/lib

    
    # fusion
    local perception_fusion_out="${OUT_ROOT}/${perception_fusion_path}"
    [ -d "${perception_fusion_out}/lib" ] || mkdir -p ${perception_fusion_out}/lib
    # 调用 copy_lib_from_dir 函数，从 TOP_DIR/bazel-bin/perception_fusion_path 路径复制库文件到 perception_fusion_out/lib 目录
    copy_lib_from_dir ${TOP_DIR}/bazel-bin/${perception_fusion_path} ${perception_fusion_out}/lib
    cp -rf "${TOP_DIR}/${perception_fusion_path}/dag" ${perception_fusion_out}
    cp -rf "${TOP_DIR}/${perception_fusion_path}/conf" ${perception_fusion_out}


    #usecase
    local perception_usecase_out="${OUT_ROOT}/${perceptiio_usecase_path}"
    [ -d "${perception_usecase_out}/lib" ] || mkdir -p ${perception_usecase_out}/lib
    # 调用 copy_lib_from_dir 函数，从 TOP_DIR/bazel-bin/perceptiio_usecase_path 路径复制库文件到 perception_usecase_out/lib 目录
    copy_lib_from_dir ${TOP_DIR}/bazel-bin/${perceptiio_usecase_path} ${perception_usecase_out}/lib

    cp -rf "${TOP_DIR}/${perceptiio_usecase_path}/dag" ${perception_usecase_out}
    cp -rf "${TOP_DIR}/${perceptiio_usecase_path}/conf" ${perception_usecase_out}

}

# 构建中间件
function build_middleware() {
    # 使用 bazel 命令行工具来构建中间件项目，//middleware/... 表示中间件项目中所有可以被构建的规则
    # --@rules_cuda//cuda:enable=True 是对 Bazel 的构建选项进行配置，启用 CUDA 支持
    bazel build //middleware/... --@rules_cuda//cuda:enable=True
}

# 发布中间件
function release_middleware() {
    # 发布设备服务
    release_middleware_device_service_func
    # 发布协议栈
    release_middleware_protocol_func
}

# 构建所有项目
function build_all() {
    release_protobuf
    release_airosrt
    export CUDA_PATH=/opt/cuda
    build_base
    build_middleware
    build_modules

    echo "build all done!"    
}

# 发布所有项目
function install_all() {
    release_base
    release_modules
    release_middleware

    echo "install all done!"
}

# 发布库文件到指定的输出目录
function release_out {
    # 定义输出目录
    local out_dir=${OUT_3RD}

    # 检查输出目录是否存在，如果不存在则创建
    [ -d "${out_dir}" ] || mkdir -p ${out_dir}

    # 遍历 /opt 目录下的所有文件和文件夹
    for lib in $(ls /opt)
    do
        # 如果在 /opt/${lib}/lib 目录存在，则执行以下操作
        if [ -e "/opt/${lib}/lib" ]; then
            # 在 /opt/${lib}/lib 目录下查找所有以 "lib*.so*" 结尾的文件，并复制到 out_dir 目录下
            find /opt/${lib}/lib/ -name "lib*.so*" | grep -v stubs | xargs -I {} cp -f {} ${out_dir}/
        fi
    done

    # 获取当前工作空间的路径
    local workspace_path=$(dirname $(readlink -f "$0"))

    # 通过 md5sum 命令计算工作空间的 MD5 哈希值
    local workspace_md5_val=`echo -n ${workspace_path} | md5sum | cut -d " " -f1`

    #定义 Bazel 缓存路径
    local bazel_cache_path="${HOME}/.cache/bazel/_bazel_${USER}"

    #定义 Bazel 缓存内的库文件路径
    local bazel_cache_lib_path="${bazel_cache_path}/${workspace_md5_val}/execroot/airos/bazel-out/k8-fastbuild/bin/_solib_k8/"

    # 在 bazel_cache_lib_path 目录下查找所有以 "lib*.so*" 结尾的文件，并复制到 out_dir 目录下
    find ${bazel_cache_lib_path} -name "lib*.so*" | xargs -I {} cp -f {} ${out_dir}/

    echo "release done."
}

# 发布monitor 模块的库文件和单元测试
function release_monitor {
    # 使用 Bazel 编译 monitor 模块的库文件和单元测试
    bazel build air_service/modules/monitor:libmonitor.so
    bazel build air_service/modules/monitor:monitor_ut

    # 定义输出目录的临时变量
    local tmp_out="${OUT_ROOT}/air_service/modules/monitor"

    # 检查输出目录是否存在，如果不存在则创建
    local tmp_out_lib="${tmp_out}/lib"
    [ -d "${tmp_out_lib}" ] || mkdir -p ${tmp_out_lib}

    # 定义库文件的路径变量
    local so_path="${TOP_DIR}/bazel-bin/air_service/modules/monitor/libmonitor.so"

    # 如果库文件存在，则复制到输出目录
    if [ -e "${so_path}" ];then
        cp -f ${so_path} ${tmp_out_lib}
    fi

    # 定义单元测试文件的临时输出目录
    local tmp_out_ut="${tmp_out}/ut"

    # 检查输出目录是否存在，如果不存在则创建
    [ -d "${tmp_out_ut}" ] || mkdir -p ${tmp_out_ut}

    # 将编译好的 monitor_ut 文件复制到输出目录
    cp -f ${TOP_DIR}/bazel-bin/air_service/modules/monitor/monitor_ut ${tmp_out_ut}
}

# 清理构建输出目录
function clean_func() {
    local out_dir="${TOP_DIR}/output"

    # 调用 Bazel 的 clean 命令，清理构建输出目录
    bazel clean
    if [ -d $out_dir ]; then
        rm -rf $out_dir
    fi
    echo "clean done!"
}

# 运行单元测试
function run_ut() {
    # 进入 TOP_DIR 目录，并将该目录加入到当前 shell 的环境变量中，> /dev/null 表示将命令输出重定向到 /dev/null，即忽略输出
    pushd ${TOP_DIR} > /dev/null
        # 运行 setup.bash 脚本，该脚本应该设置了一些环境变量
        source setup.bash
    popd

    run_base_device_connect_ut

    echo "ut done!"
}

function main() {
    if [ "$#" -eq 0 ]; then # $#参数个数为0，退出
        exit 0;
    fi

    env_setup

    local cmd="$1" 
    shift
    case "${cmd}" in
        build)
            build_all
            release_monitor
            install_all
            ;;
        test)
            build_all
            install_all
            release_out
            run_ut
            ;;
        clean)
            clean_func
            ;;
        release)
            build_all
            release_monitor
            install_all
            release_out
            ;;
        *)
            ;;
    esac
}

main "$@" # 传入$@所有参数，执行main