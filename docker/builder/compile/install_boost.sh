#!/usr/bin/env bash

cd "$(dirname "${BASH_SOURCE[0]}")"
. ./installer_base.sh

apt_get_update_and_install \
    liblzma-dev \
    libbz2-dev \
    libzstd-dev

# Ref: https://www.boost.org/
VERSION="1_74_0"

PKG_NAME="boost_${VERSION}.tar.bz2"
DOWNLOAD_LINK="https://boostorg.jfrog.io/artifactory/main/release/${VERSION//_/.}/source/boost_${VERSION}.tar.bz2"
CHECKSUM="83bfc1507731a0906e387fc28b7ef5417d591429e51e788417fe9ff025e116b1"

download_if_not_cached "${PKG_NAME}" "${CHECKSUM}" "${DOWNLOAD_LINK}"

tar xjf "${PKG_NAME}"

py3_ver="$(py3_version)"

# Ref: https://www.boost.org/doc/libs/1_73_0/doc/html/mpi/getting_started.html
pushd "boost_${VERSION}"
    # A) For mpi built from source
    #  echo "using mpi : ${SYSROOT_DIR}/bin/mpicc ;" > user-config.jam
    # B) For mpi installed via apt
    # echo "using mpi ;" > user-config.jam
    ./bootstrap.sh \
        --with-python-version=${py3_ver} \
        --prefix="${SYSROOT_DIR}" \
        --without-icu

    ./b2 -d+2 -q -j$(nproc) \
        --without-graph_parallel \
        --without-mpi \
        variant=release \
        link=shared \
        threading=multi \
        install
        #--user-config=user-config.jam
popd

# Clean up
rm -rf "boost_${VERSION}" "${PKG_NAME}"

if [[ -n "${CLEAN_DEPS}" ]]; then
    apt_get_remove  \
        liblzma-dev \
        libbz2-dev \
        libzstd-dev
fi

