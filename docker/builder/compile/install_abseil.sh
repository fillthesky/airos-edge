#!/usr/bin/env bash

set -e

cd "$(dirname "${BASH_SOURCE[0]}")"
. ./installer_base.sh

# Install abseil.
VERSION="20200225.2"
PKG_NAME="abseil-cpp-${VERSION}.tar.gz"
DOWNLOAD_LINK="https://apollo-system.cdn.bcebos.com/archive/6.0/${VERSION}.tar.gz"
CHECKSUM="f41868f7a938605c92936230081175d1eae87f6ea2c248f41077c8f88316f111"

download_if_not_cached "${PKG_NAME}" "${CHECKSUM}" "${DOWNLOAD_LINK}"

DEST_DIR="/opt/"

tar xzf "${PKG_NAME}"
pushd "abseil-cpp-${VERSION}"
    mkdir build && cd build
    cmake .. \
        -DBUILD_SHARED_LIBS=ON \
        -DCMAKE_CXX_STANDARD=14 \
        -DCMAKE_INSTALL_PREFIX=${DEST_DIR}
    cmake --build . --target install
popd

# Clean up
rm -rf "abseil-cpp-${VERSION}" "${PKG_NAME}"
