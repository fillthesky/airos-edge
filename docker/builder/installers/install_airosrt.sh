#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_airosrt {
    local url="https://zhilu.bj.bcebos.com/airosrt.tar.gz"
    local sha256="a9d5d46fab2503066de9f2469f606fb117904f452c1eed39f6e3871e3f0a80fb"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "airosrt.tar.gz" "${sha256}"; then
            tar -zxf airosrt.tar.gz
            python3 -m pip install --timeout 30 --no-cache-dir protobuf 
        else
            echo "check_sha256 failed: airosrt.tar.gz"
        fi
        rm airosrt.tar.gz
    popd >/dev/null
}

function main {
    install_airosrt
}

main "$@"
