#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_glfw {
    local url="https://zhilu.bj.bcebos.com/glfw.tar.gz"
    local sha256="240647c81697f6bce27758475c63fdfdd745b297905f4e90a037ba9af1da5b69"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "glfw.tar.gz" "${sha256}"; then
            tar -zxf glfw.tar.gz
        else
            echo "check_sha256 failed: glfw.tar.gz"
        fi
        rm glfw.tar.gz
    popd >/dev/null
}

function main {
    install_glfw
}

main "$@"