#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_boost {
    local url="https://zhilu.bj.bcebos.com/boost.tar.gz"
    local sha256="7d88ba26153d20f074d3af140fa0fb5aa2a0c1d5db3fb3821a0493f5086edd16"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "boost.tar.gz" "${sha256}"; then
            tar -zxf boost.tar.gz
        else
            echo "check_sha256 failed: boost.tar.gz"
        fi
        rm boost.tar.gz
    popd >/dev/null
}

function main {
    install_boost
}

main "$@"