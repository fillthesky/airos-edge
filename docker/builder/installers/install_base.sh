#!/usr/bin/env bash

set -e

function check_sha256() {
    local pkg="$1"
    local expected_cs="$2"
    local actual_cs=$(/usr/bin/sha256sum "${pkg}" | awk '{print $1}')
    if [[ "${actual_cs}" == "${expected_cs}" ]]; then
        true
    else
        echo "${pkg}: "
        echo "expect: ${expected_cs}"
        echo "actual: ${actual_cs}"
        false
    fi
}