# Build_Guide.md

## 1 执行编译
build_airos.sh脚本用于airos项目编译和发布，使用方式：
* 构建项目：bash build_airos.sh build
* 构建并发布项目：bash build_airos.sh release
* 构建、发布项目并运行所有单元测试：bash build_airos.sh test
* 清理发布产出：bash build_airos.sh clean
## 2 编译脚本解析
airos基于Bazel进行编译，Bazel的详细使用请参考https://bazel.build
### 2.1 Bazel快速入门
下面简单介绍项目中用到的Bazel基础知识
### 2.1.1 构建文件
WORKSPACE文件，用于将目录及其内容标识为 Bazel工作区，位于项目目录结构的根目录下

![WORKSPACE](./images/complie/workspace.png)

BUILD文件，用于告知 Bazel如何构建项目的不同部分。工作区中包含一个 BUILD 文件的目录就是一个软件包
### 2.1.2 编译规则

**cc_library**

以cloud_device模块为例，BUILD文件如下：

![cc_library](./images/complie/cc_library.png)

* name: so的名字，产出为libname.so
* srcs:  一般为源文件，也可以是头文件
* hdrs: 头文件，库的公共接口
* deps：依赖的动态或者静态库，@代表全局 :代表本软件包  //和@一样，代表绝对路径
* linkopts: 链接选项
* linkstatic = True 创建静态库，类似linkopts=['-static‘]
* linkstatic = False 创建动态库，类似linkopts=[‘ -shared‘]，但是也会产生静态库
* visibility public代表对别的软件包可见

**cc_binary**

以信号灯测试工具为例，BUILD文件如下：

![cc_binary](./images/complie/cc_binary.png)

和cc_library部分标签一致，但是没有hdrs标签
区别：linkstatic：默认优先关联静态库，linkstatic=False会关联动态库

**cc_test**

![cc_test](./images/complie/cc_test.png)

和cc_binary基本一致

**cc_binary产出so**

![cc_binary产出so](./images/complie/cc_binary_so.png)

不依赖其他库的so可以直接使用cc_library产出
由于cc_library并不会强制寻找所有符号，假如该so还依赖其他的so，需要开发者在linkopts里面加-lxxx，和-Lxxx，如果不加，可以编译通过，但运行可能会找不到符号。
因此，可以直接使用cc_binary产出so的方式编译动态库，使用cc_binary规则时，Bazel会确保所有符号都被解析，动态库会自动加上相关库的依赖。

### 2.1.3 编译

![complie](./images/complie/complie.png)

例如以上模块，可以使用下面的命令编译
bazel build base/device_connect/cloud:cloud_device
最终编译的产出

![release](./images/complie/release.png)

* bazel-airos指向了一个镜像的项目文件夹，实际文件软连接到了项目文件
* bazel-out目录包含了构建过程的中间产物
* bazel-bin目录包含了构建完成的最终输出文件
### 2.1.4 产出
当指定deps标签时，bazel会把deps里的库都编译成自定义的文件名，最后ldd bin或者so的时候，指向的也是这些文件名

![deps](./images/complie/deps.png)

Bazel会把所有deps的库都放到一个以md5命名的文件夹
airos编译脚本build_airos.sh中会将项目依赖的所有so拷贝到输出目录，最终使用setup.bash把相关文件夹环境变量即可找到项目启动所需的全部so
![output](./images/complie/output.png)

## 2.2 集成编译脚本
build_airos.sh脚本包含了airos所有模块的编译和产出
每个模块的操作主要包括两部分，使用bazel build进行构建，将编译产出发布到output文件夹，另外编译脚本包含了第三方库的产出发布。
### 2.2.1 模块构建

![build](./images/complie/build.png)

* release_protobuf用于编译protobuf工具和依赖
* release_airosrt用于发布中间件相关的工具和配置
* build_base用于构建base层
* build_middleware用于构建中间件，包括协议栈等
* build_modules用于构建智路OS模块
### 2.2.2 模块发布

![module_release](./images/complie/module_release.png)

如果开发者需要替换自己的模块，可能需要调整BUILD和集成编译脚本，如果增加模块，需要在集成脚本中添加模块的构建和发布操作。
下面以替换cloud模块为例，介绍调整流程：
* 修改base/device_connect/cloud/BUILD，在cc_library中添加自定义设备的编译文件和相关的依赖库
* 确认设备服务中middleware/device_service/cloud/BUILD是否需要修改为自定义设备
* 确认build_airos.sh脚本中的build_base_device_connect和build_middleware_device_service是否需要调整
### 2.2.3 第三方库
第三方库的Bazel描述文件存放在third_party下，提供给airos依赖。
大部分无需经常改变的第三方库，如cuda、paddlepaddle、boost等会在镜像集成时下载/编译，并且存放在镜像的/opt目录下
少部分可能有版本变动的第三方库，如glog、protobuf等会在描述中添加url，在工程编译时下载。
**第三方库介绍**
| 库名         | 版本                                   | 描述                                         |
|--------------|----------------------------------------|----------------------------------------------|
| abseil_cpp   | 20200225.2                             | C++标准库扩展，用于提供更多的C++工具和实用功能   |
| cyberRT      | 13ea72d                                | 实时通信中间件，apollo开源的cyberRT             |
| atlas        | 3.10.3-5                               | 线性代数库，用于优化数学运算，特别是在不同硬件架构上 |
| boost        | 1.74.0                                 | 开源C++库，提供了各种各样的工具和数据结构         |
| cuda         | 10.2.89                                | 并行计算库，用于加速GPU计算                     |
| eigen        | 3.3.7                                  | 用于线性代数运算的C++模板库                     |
| fastrtps     | 1.5.0                                  | 实时通信中间件库，用于支持快速消息传递           |
| glew         | 2.1.0                                  | OpenGL扩展包，用于管理OpenGL扩展功能            |
| gflags       | 2.2.2                                  | 命令行标志处理库，用于处理命令行参数             |
| glog         | 0.4.0                                  | C++日志库，用于记录应用程序的日志                |
| gtest        | 1.10.0                                 | C++测试框架，用于编写单元测试                   |
| ffmpeg       | 4.3.1                                  | 开源多媒体框架，用于处理音频和视频数据           |
| glfw         | 3.2.1-1                               | 用于创建窗口和OpenGL上下文的库                   |
| opencv       | 4.4.0                                  | 计算机视觉和图像处理的开源计算机视觉库           |
| protobuf     | 3.14.0                                 | 协议缓冲区库，用于序列化结构化数据               |
| paho         | 1.3.11                                 | 支持MQTT协议的开源客户端库                       |
| tensorrt     | 7.1.3.4.Ubuntu-18.04.x86_64-gnu.cuda-10.2.cudnn8.0 | 深度学习推理库，用于高性能深度学习推理 |
| pugixml      | 1.12.1                                 | 轻量级的C ++XML操作库                          |
| yaml_cpp     | 0.7.0                                  | C++库，用于解析和生成YAML格式的数据             |

**第三方库依赖**

在模块的BUILD描述中，使用第三方库的名称和路径进行依赖导入，例如：

![third](./images/complie/third.png)

以上模块依赖了protobuf、glog、fastrtps、gflags、airosrt、yamlcpp

**第三方库产出**

在build_airos.sh脚本中使用release_out搜索第三方库并将其拷贝到输出目录，便于依赖集中
包括拷贝/opt下的所有第三方库，以及bazel缓存的中间编译依赖。

**第三方库编译**

目前airos提供的镜像为x84_64版本镜像，目录docker/builder/installers存放的install脚本为第三方库的安装脚本
在开发者使用airos-edge/docker/builder/ubuntu18.04_x86_64.dockerfile进行docker镜像构建时，install脚本执行时会拉取已编译完成的x84_64版本第三方库进行安装，并且构建x84_64版本镜像。
如果需要编译其他平台版本，需要使用第三方库的编译脚本构建相关平台的第三方依赖产出。
docker/builder/compile提供了部分airos第三方库的示例编译脚本。开发者可以根据2.2.3所描述的版本进行相关平台的编译