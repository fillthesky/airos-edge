# Perception_Pipeline_QuickStart

## 更新内容
| 功能             | 文档链接                                                     |
| ---------------- | ------------------------------------------------------------ |
| 单相机感知       | [Perception Camera Pipeline QuickStart Guide](./Perception_Camera_Pipeline.md)|
| 多传感器融合       | [Perception Fusion Pipeline QuickStart Guide](./Perception_Fusion_Pipeline.md)|
| 事件检测      | [Perception Usecase Pipeline QuickStart Guide](./Perception_Usecase_Pipeline.md)|

## 快速开始
1. 拉取代码到本地
2. 拉取docker镜像并启动容器
    ```
    bash docker/scripts/docker_start.sh
    ```
3. 进入docker容器
    ```
    bash docker/scripts/docker_into.sh
    ```
4. 编译且发布
    ```
    bash build_airos.sh release
    ```
5. 配置运行环境
    ```
    source setup.bash
    ```
6. 运行pipeline
    ```
    cyber_launch start air_service/launch/perception_pipeline.launch
    ```
7. 单模块可视化
    - 运行感知可视化
    ```
    cyber_launch start air_service/launch/camera_visualization.launch
    ```
    - 运行多相机融合可视化
    ```
    cyber_launch start air_service/launch/fusion_visualization.launch
    ```
    - 运行 usecase 可视化
    ```
    cyber_launch start air_service/launch/usecase_visualization.launch
    ```
    - 修改红绿灯配置:
    ```
    红绿灯配置模拟器配置需要根据具体的路口进行设置，以下设置例子为广州 76 号路口，所设置的红绿灯周期为随机设置：

    # middleware/device_service/traffic_light/conf/phase_to_direction.json
    {
        "phase": [1, 2, 3, 4, 7, 8, 9, 10, 18, 19, 20],
        "direction" :[6, 16, 26, 36, 7, 17, 27, 37, 19, 29, 39],
        "round" :[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    }

    # base/device_connect/traffic_light/simulator/signal_scheme.json
    {
	    "0": {
		    "4": {
			    "type": 1,
			    "step": [[3, 50], [1, 29], [2, 3]]
		    },
		    "10": {
			    "type": 2,
			    "step": [[3, 50], [1, 29], [2, 3]]
		    },
		    "20": {
			    "type": 9,
			    "step": [[1, 29], [2, 3], [3, 50]]
		    }
	    },
	    "90": {
		    "1": {
			    "type": 1,
			    "step": [[1, 29], [2, 3], [3, 50]]
		    },
		    "7": {
			    "type": 2,
			    "step": [[3, 50], [2, 3], [1, 29]]
		    } 
	    },
	    "180": {
		    "2": {
			    "type": 1,
			    "step": [[3, 50], [1, 29], [2, 3]]
		    },
		    "8": {
			    "type": 2,
			    "step": [[1, 29], [2, 3], [3, 50]]
		    },
		    "18": {
			    "type": 9,
			    "step": [[3, 50], [1, 29], [2, 3]]
		    }
	    },
	    "270": {
		    "3": {
			    "type": 1,
			    "step": [[3, 50], [1, 29], [2, 3]]
		    },
		    "9": {
			    "type": 2,
			    "step": [[3, 50], [1, 29], [2, 3]]
		    },
		    "19": {
			    "type": 9,
			    "step": [[3, 50], [1, 29], [2, 3]]
		    }
	    }
    }
    ```
	- 运行红绿灯模块
	```
	cyber_launch air_service/launch/traffic_light_pipeline.launch
	```

    - 启动红绿灯模拟器:
    ```
	cd base/device_connect/traffic_light/simulator && python3 gat1743_simulator.py
    ```