# Perception Camera Pipeline


## 1.单相机感知框图
TODO
## 2.快速开始
### 2.1 启动相机感知程序
#### 2.1.1 使用dag文件单独启动
编译完成获得产出后，可以使用`perception_camera_123.dag`单独拉起相机感知的处理流程。
执行以下命令：
```bash
source /airos-edge/setup.bash # 初始化运行环境
nohup mainboard -d /airos-edge/output/air_service/modules/perception-camera/dag/perception_camera_123.dag -p perception_camera & # 拉起处理流程
```
#### 2.1.2 使用launch文件启动
编译完成获得产出后，可以编写launch文件启动，以./air_service/launch/perception_pipeline.launch文件为例，在launch文件中加入以下配置：
<module>
    <name>perception_camera</name>
    <dag_conf>/airos/air_service/modules/perception-camera/dag/perception_camera_123.dag</dag_conf>
    <process_name>perception_camera</process_name>
</module>
执行以下命令：
```bash
source /airos-edge/setup.bash # 初始化运行环境
nohup cyber_launch start /airos-edge/air_service/launch/perception_pipeline.launch & # 拉起处理流程
```

### 2.2 启动相机感知可视化程序
#### 2.2.1 使用dag文件单独启动
编译完成获得产出后，可以使用`perception_camera_123_viz.dag`单独拉起相机感知的处理流程。
执行以下命令：
```bash
source /airos-edge/setup.bash # 初始化运行环境
nohup mainboard -d /airos-edge/output/air_service/modules/perception-camera/dag/perception_camera_123_viz.dag -p perception_camera_viz & # 拉起处理流程
```
#### 2.2.2 使用launch文件启动
编译完成获得产出后，可以编写launch文件启动，以./air_service/launch/camera_visualization.launch文件为例，在launch文件中加入以下配置：
<module>
    <name>perception_camera</name>
    <dag_conf>/airos/air_service/modules/perception-camera/dag/perception_camera_123_viz.dag</dag_conf>
    <process_name>perception_camera</process_name>
</module>
执行以下命令：
```bash
source /airos-edge/setup.bash # 初始化运行环境
nohup cyber_launch start /airos-edge/air_service/launch/camera_visualization.launch & # 拉起处理流程
```

## 3.模块介绍
相机感知模块，主要功能为接收IP相机RTSP视频流，解码成RGB图片，通过算法识别出视频中的障碍物，并转换到世界坐标系．
模块由四个子模块组成，包括检测 跟踪　roi过滤　回３d．　
### 3.1 检测：
利用深度学习算法，识别出物体类别＼２Ｄ框＼长宽高＼朝向角＼底面中心点图像坐标等信息．输入图像首先经过yolov3的backbone进行特征提取，2D阶段输出目标的类别、置信度以及2D框，3D阶段输出目标的长宽高、朝向角以及物体底面中心点的2D坐标。
### 3.2 ROI过滤：
感兴趣区域(ROI) 是从图像中选择的一个图像区域，这个区域是你的图像分析所关注的重点．ROI过滤模块主要功能是过滤掉感兴趣区域外的物体．
### 3.3 跟踪：
其主要任务是给定一个图像序列，找到图像序列中运动的物体，并将不同帧的运动物体进行识别，也就是给定一个确定准确的ID,这些物体可以是任意的，物体可以是人、车辆、锥桶、三角牌、各种动物等目标类型。跟踪算法针对每一帧检测数据进行跟踪信息维护。在检测之前，跟踪算法会对T-1时刻的跟踪障碍物进行属性预测；当检测数据到来时跟踪会根据检测目标的属性和T-1时刻的是跟踪数据进行数据关联，根据关联处理的信息，确定跟踪队列数据以及新出现的检测目标进行跟踪队列的创建。之后根据关联信息以及跟踪队列对关联好的目标进行属性处理和更新，之后跟踪队列中成功跟踪的目标输出给下游使用；跟踪队列中维护的目标进入下一轮跟踪循环。跟踪关联采用的度量方式可以自定义，目前流行的方式有kalman预测框和当前帧的检测框之间计算相似度量指标、IOU、点迹势函数、deep learning方法等，研究者可以根据问题的锚点灵活选择。
### 3.4 回３d：
根据模型预测的图像坐标系下的底面中心点的图像坐标（u，v），利用地面方程和相机内参，计算得到相机坐标系下的底面中心点的 3D坐标(X, Y, Z)，然后利用模型预测得到的物体高度h，计算得到物体中心点在相机坐标系下的3D坐标.

>如果您需要使用智路OS开发者平台，或者在开发使用过程中遇到任何问题，欢迎通过以下方式联系我们：  
> Email：zhiluos@163.com