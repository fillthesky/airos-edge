/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <string>
#include "air_middleware_component.h"
#include "middleware/protocol/proto/v2xpb-asn-message-frame.pb.h"
#include "middleware/protocol/proto/v2xpb-config-event-details.pb.h"
#include "air_service/modules/proto/usecase.pb.h"
#include "middleware/protocol/rsi_generator/affect_path/v2x_affect_path.h"

namespace os {
namespace v2x {
namespace protocol {

class AIROS_COMPONENT_CLASS_NAME(RsiGeneratorComponent)
    : public airos::middleware::ComponentAdapter<
          airos::usecase::EventOutputResult> {
 public:
  typedef std::shared_ptr<const v2xpb::rscu::config::RteEventDetail> RteEventDetailPtr;

  AIROS_COMPONENT_CLASS_NAME(RsiGeneratorComponent)(){};

  virtual ~AIROS_COMPONENT_CLASS_NAME(
      RsiGeneratorComponent)() override{};

  bool Init() override;
  bool Proc(const std::shared_ptr<const airos::usecase::EventOutputResult>&
                event_ptr) override;
 private:
   void GenerateRsiMsg(v2xpb::asn::Rsi* rsi_pb, int msg_cnt);
   bool GetRsuMap(const std::string& rsu_map);
   std::string rscu_sn_;
   static int msg_cnt_;
   std::shared_ptr<v2xpb::asn::MessageFrame> asn_map_data_;
   std::shared_ptr<v2xpb::asn::MessageFrame> asn_pb_data_;
   int zone_;
   double cross_lat_ = 0.0;
   double cross_lon_ = 0.0;
   std::string city_string_ = "yzhu#"; // length 5 byte
   int rsu_intersection_id_ = 10;
   std::shared_ptr<RsiAffectPath> affect_path_;
   v2xpb::rscu::config::EventDetails conf_;
   std::map<std::string, RteEventDetailPtr> ev_map_;
};

REGISTER_AIROS_COMPONENT_CLASS(RsiGeneratorComponent,
                               airos::usecase::EventOutputResult);

}  // namespace protocol
}  // namespace v2x
}  // namespace os
