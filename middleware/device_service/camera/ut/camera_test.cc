/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include <functional>
#include <iostream>
#include <map>
#include <string>
#include <thread>

#include "camera/interface/camera_receiver.h"

void run(const std::pair<std::string, std::string>& config) {
  auto& rec = os::v2x::device::CameraImageReceiver::Instance();
  if (!rec.Init(config.second, 0)) {  // gpu_id = 0
    std::cout
        << "os::v2x::device::CameraImageReceiver::Instance().Init() failed"
        << std::endl;
  };

  std::cout << config.first << ": thread runing" << std::endl;

  while (true) {
    auto res = rec.GetCameraData(config.first);
    if (res) {
      std::cout << res->camera_name << ": " << res->height << " x "
                << res->width << " : " << res->timestamp << std::endl;
    }
  }
}

int main(int argc, char** argv) {
  if (argc < 2 || argc % 2 != 1) {
    std::cout << "Usage: " << argv[0]
              << " camera1 config_file_path1 camera2 config_file_path2 ..."
              << std::endl;
    return -1;
  }

  std::vector<std::thread> works;

  for (int i = 1; i < argc; i += 2) {
    std::string camera_name(argv[i]);
    std::string config_file_path(argv[i + 1]);
    works.emplace_back(
        std::bind(run, std::make_pair(camera_name, config_file_path)));
  }

  for (auto& x : works) {
    x.join();
  }

  std::cout << "over" << std::endl;
  return 0;
}
