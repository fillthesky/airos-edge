/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

// #include <Eigen/Dense>
// #include <Eigen/Core>
#include <atomic>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include "air_middleware_component.h"
#include "middleware/device_service/camera/camera_service.h"
#include "air_service/modules/perception-camera/pipeline/obstacle_camera_perception.h"
#include "air_service/modules/perception-camera/proto/perception_camera.pb.h"
#include "air_service/modules/proto/perception_obstacle.pb.h"

namespace airos {
namespace perception {
namespace camera {

#define CAMERA_DETECTION_COMPONENT \
  AIROS_COMPONENT_CLASS_NAME(CameraPerceptionComponent)

class CAMERA_DETECTION_COMPONENT
    : public airos::middleware::ComponentAdapter<> {
 public:
  CAMERA_DETECTION_COMPONENT() = default;
  ~CAMERA_DETECTION_COMPONENT();

  CAMERA_DETECTION_COMPONENT(const CAMERA_DETECTION_COMPONENT &) = delete;
  CAMERA_DETECTION_COMPONENT &operator=(const CAMERA_DETECTION_COMPONENT &) =
      delete;

  bool Init() override;

 private:
  void ProcThread();

  bool InitConfig();
  bool InitCameraDriver();
  bool InitAlgorithmPipeline();
  void SerializeMsg(PerceptionFrame &frame, PerceptionObstacles &out_msg);

 private:
  std::unique_ptr<std::thread> proc_thread_;
  std::atomic<bool> running_{false};

  std::unique_ptr<middleware::device_service::CameraService> camera_;
  // camera::CameraPerceptionInitParam camera_perception_init_options_;
  std::unique_ptr<camera::ObstacleCameraPerception> camera_obstacle_pipeline_;
  PerceptionParam perception_param_;
  // std::string camera_name_ = "";
};

REGISTER_AIROS_COMPONENT_NULLTYPE_CLASS(CameraPerceptionComponent);

}  // namespace camera
}  // namespace perception
}  // namespace airos
