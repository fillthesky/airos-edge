syntax = "proto2";
package airos.perception;

import "air_service/modules/proto/header.proto";
import "air_service/modules/proto/perception_common.proto";

enum PerceptionErrorCode {
    ERROR_NONE = 0;         // no error
    ERROR_TF = 1;           // return result error
    ERROR_PROCESS = 2;      // process error 
    ERROR_UNKNOWN = 3;      // unknow error
}

message Point {
    optional double x = 1;  // in meters.
    optional double y = 2;  // in meters.
    optional double z = 3;  // height in meters.
}

enum SubType {
    UNKNOWN = 0;
    UNKNOWN_MOVABLE = 1;
    UNKNOWN_UNMOVABLE = 2;
    CAR = 3;
    VAN = 4;
    TRUCK = 5;
    BUS = 6;
    CYCLIST = 7;
    MOTORCYCLIST = 8;
    TRICYCLIST = 9;
    PEDESTRIAN = 10;
    TRAFFICCONE = 11;
    SAFETY_TRIANGLE = 12;
    MAX_OBJECT_TYPE = 13;
    BARRIER_DELINEATOR = 14;
    BARRIER_WATER = 15;
}

enum OcclusionState {
    OCC_UNKNOWN = 0;            
    OCC_NONE_OCCLUDED = 1;          // 无拥堵
    OCC_PARTIAL_OCCLUDED = 2;       // 部分拥堵
    OCC_COMPLETE_OCCLUDED = 3;      // 完全拥堵
}

enum TruncationState {
    TRUNC_UNKNOWN = 0;
    TRUNC_TRUE = 1;                 // 有截断
    TRUNC_FALSE = 2;                // 无阶段
}

message SensorMeasurement {
    optional string sensor_id = 1;
    optional int32 id = 2;

    optional Point position = 3;
    optional double theta = 4;
    optional double length = 5;
    optional double width = 6;
    optional double height = 7;

    optional Point velocity = 8;

    enum Type {
        UNKNOWN = 0;
        UNKNOWN_MOVABLE = 1;
        UNKNOWN_UNMOVABLE = 2;
        PEDESTRIAN = 3;  // Pedestrian, usually determined by moving behaviour.
        BICYCLE = 4;  // bike, motor bike
        VEHICLE = 5;  // Passenger car or truck.
    };
    optional Type type = 9;
    optional SubType sub_type = 10;
    optional double timestamp = 11;
    optional BBox2D box = 12; // only for camera measurements
    optional bool velocity_converged = 13;
    optional OcclusionState occ_state_2d = 14 [default = OCC_UNKNOWN];
    // existance confidence, range [0, 1], default 1.
    optional float confidence = 15 [default = 1];
    // whether obj is a parse obj
    optional bool is_parse_obj = 16 [default = false]; 
}

message PerceptionObstacle {
    optional int32 id = 1;  // obstacle ID.
    optional Point position = 2;  // obstacle position in the world coordinate system.
    optional double theta = 3;  // heading in the world coordinate system.
    optional double theta_variance = 4;
    optional Point velocity = 5;  // obstacle velocity.

    // Size of obstacle bounding box.
    optional double length = 6;  // obstacle length.
    optional double width = 7;  // obstacle width.
    optional double height = 8;  // obstacle height.

    repeated Point polygon_point = 9;  // obstacle corner points.
    optional double tracking_time = 10;  // duration of an obstacle since detection in s.

    enum Type {
        UNKNOWN = 0;
        UNKNOWN_MOVABLE = 1;
        UNKNOWN_UNMOVABLE = 2;
        PEDESTRIAN = 3;  // Pedestrian, usually determined by moving behaviour.
        BICYCLE = 4;  // bike, motor bike
        VEHICLE = 5;  // Passenger car or truck.
    };
    optional Type type = 11;  // obstacle type

    optional double timestamp = 12;  // GPS time in seconds.

    // Just for offline debuging, onboard will not fill this field.
    // Format like : [x0, y0, z0, x1, y1, z1...]
    repeated double point_cloud = 13 [packed = true];

    // a stable obstacle point in the world coordinate system
    // position defined above is the obstacle boundingbox ground center
    optional Point anchor_point = 14;

    // position covariance which is a row-majored 3x3 matrix
    // repeated double position_covariance = 15 [packed = true];

    // orthogonal distance between obstacle lowest point and ground plane
    optional double height_above_ground = 15 [default = nan];

    optional BBox2D bbox2d = 16;                  // 2D 检测框
    repeated Point  cube_pts8 = 17;               // 8点 3D框
    optional SubType sub_type = 18;               // 障碍物子类型
    repeated SensorMeasurement measurements = 19; // sensor measurements

    optional Point acceleration = 20;  // obstacle acceleration.
    // acceleration covariance which is a row-majored 3x3 matrix

    optional OcclusionState occ_state = 21 [default = OCC_UNKNOWN];
    optional TruncationState trunc_state = 22 [default = TRUNC_UNKNOWN];

    optional bool velocity_converged = 23 [default = true];

    message TypeProb {
        optional Type type = 1;
        optional float prob = 2;
    }

    message SubTypeProb {
        optional SubType sub_type = 1;
        optional float prob = 2;
    }
    
    repeated TypeProb type_probs = 24;
    repeated SubTypeProb sub_type_probs = 25;
    optional int32 sub_type_id = 26; // obstacle type id
    optional double sub_type_id_confidence = 27;

}

enum ShadowType {
    SHADOW_DYNAMIC = 0;
    SHADOW_STATIC = 1;
}

message ShadowRegion {
    optional int32 id = 1; // shadow region ID
    repeated int32 obstacle_id = 2; // obstacles id
    // polygon of shadow region
    repeated Point polygon_point = 3;
    optional ShadowType shadow_region_type = 4 [default = SHADOW_DYNAMIC]; // shadow region type: static or dynamic
}

message PerceptionObstacles {
    repeated PerceptionObstacle perception_obstacle = 1;  // An array of obstacles
    optional airos.header.Header header = 2;         // Header
    optional PerceptionErrorCode error_code = 3 [default = ERROR_NONE]; //Error code
    // shadow region of obstacles                         
    repeated ShadowRegion shadow_region = 4;              // 盲区
    // An array of unobserved obstacles
    repeated PerceptionObstacle unobserved_obstacle = 5;  // TODO 在fusion 离线工具中使用
}